<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Embedded Motrain experience.
 *
 * @package    block_mootivated
 * @copyright  2021 Mootivation Technologies Corp.
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

use block_mootivated\pocket;

require(__DIR__ . '/../../config.php');

require_login(null, false);

$landingpage = optional_param('page', null, PARAM_ALPHANUMEXT);

$PAGE->set_url('/blocks/mootivated/store.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_context(context_user::instance($USER->id));
$PAGE->set_title(get_string('store', 'block_mootivated'));
$PAGE->set_heading(get_string('store', 'block_mootivated'));
$PAGE->navigation->override_active_url(new moodle_url('/user/profile.php', ['id' => $USER->id]));
$PAGE->navbar->add(get_string('store', 'block_mootivated'));

$manager = new block_mootivated\manager($USER, $PAGE->context, local_mootivated\helper::get_school_resolver());
$manager->require_view();
$manager->require_school();
$client = $manager->get_client_user();

// Gracefully redirect when legacy experience is used.
if ($manager->use_legacy_experience()) {
    if ($landingpage === 'info') {
        redirect(new moodle_url('/blocks/mootivated/info.php'));
    } else if ($landingpage === 'leaderboards') {
        redirect(new moodle_url('/blocks/mootivated/leaderboard.php'));
    } else if ($landingpage === 'purchases') {
        redirect(new moodle_url('/blocks/mootivated/purchases.php'));
    }
    redirect(new moodle_url('/blocks/mootivated/store.php'));
}

$renderer = $PAGE->get_renderer('block_mootivated');

$pocket = pocket::get($USER->id);
$key = $pocket->add('iframe_url', $landingpage);

$iframeurl = new moodle_url('/blocks/mootivated/redirect.php', [
    'key' => $key,
    'sesskey' => sesskey(),
    'returnto' => '/'
]);

echo $OUTPUT->header();

echo html_writer::tag('iframe', '', ['src' => $iframeurl->out(false),
    'style' => 'width: 100%; min-height: 800px; height: 100%; max-height: 100vh; border: none',
    ]);

echo $OUTPUT->footer();
